module.exports = {
    siteMetadata: {
        siteUrl: "https://www.yourdomain.tld",
        title: "My Gatsby Site",
    },
    plugins: [
        "gatsby-plugin-sass",
        "gatsby-plugin-react-leaflet",
    ],
};

require('dotenv').config({
    path: `.env.${process.env.NODE_ENV}`
});