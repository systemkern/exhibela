import React from "react";
import "leaflet/dist/leaflet.css";
import OverviewMap from "../components/OverviewMap";

const IndexPage = () => {
    return (
        <main>
            <title>Home Page</title>
            <hr/>
            <OverviewMap/>
        </main>
    )
}

export default IndexPage
