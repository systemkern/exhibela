export const attribution =
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'

export const tileUrl =
    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

export const defaultMapState = {
    lat: 41.3956266,
    lng: 2.1656623,
    zoom: 13,
    minZoom: 1,
}

export const schools = [
    {
        id: 1,
        name: "Multicultural Salsa Bachata Kizomba",
        address: "Carrer de Vistalegre 18, Barcelona, Spain",
        coordinates: [41.37858,2.16655],
        regularClasses: [
            {
                level : "Intermedio 2",
                day : "monday",
                start : "20:00",
                end : "21:00",
                dances : ["Bachata Sensual","Bachata"]
            },
            {
                level : "Intermedio 2",
                day : "monday",
                start : "21:00",
                end : "22:00",
                dances : ["Salsa en Linea On 1", "Salsa"]
            },
        ]
    },
    {
        id: 2,
        name: "Fun 2 Salsa Bachata Kizomba",
        address: "Carrer de Leo 10, Barcelona, Spain",
        coordinates: [41.38215,2.16496],
        regularClasses: [
            {
              level : "Beginner",
              day : "tuesday",
              start : "19:00",
              end : "20:00",
              dances : ["Bachata"]
            },
            {
              level : "Beginner",
              day : "tuesday",
              start : "20:00",
              end : "21:00",
              dances : ["Salsa Rueda de Casino", "Salsa"]
            },
        ]
    },
    {
        id: 3,
        name: "Baila Siempre Salsa Y Bachata",
        address: "Online",
        link: "https://www.meetup.com/baila-siempre-salsa-y-bachata",
        coordinates: [41.38647,2.17128],
        regularClasses: [
            {
              title: "Salsa Basico Pasitos y Figuras en Pareja",
              level : "Beginner",
              day : "tuesday",
              start : "19:00",
              end : "20:00",
              dances : ["Salsa"]
            },
        ]
    },
];