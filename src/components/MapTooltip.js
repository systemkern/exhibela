import React, { Component } from "react";

export default class MapTooltip extends Component {
    render() {
        let school = this.props.school;
        return school ? (
            <div className="resortCard">
                <p>
                    <b>#{school.id}: {school.name}</b>
                </p>
            </div>
        ) : (
            ""
        );
    }
}