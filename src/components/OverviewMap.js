import React, {Component} from "react";
import {
    Map,
    TileLayer,
    CircleMarker,
    Popup
} from "react-leaflet";
import {
    attribution,
    tileUrl,
    defaultMapState,
} from '../Data';
import MapTooltip from "./MapTooltip";
import "leaflet/dist/leaflet.css";
import {schools} from "../Data"

export default class OverviewMap extends Component {
    state = defaultMapState;

    render() {
        return schools ? (
            <Map
                center={[this.state.lat, this.state.lng]}
                zoom={this.state.zoom}
                style={{width: "100%", position: "absolute", top: 0, bottom: 0, zIndex: 500,}}
                updateWhenZooming={false}
                updateWhenIdle={true}
                preferCanvas={true}
                minZoom={this.state.minZoom}
            >
                <TileLayer
                    attribution={attribution}
                    url={tileUrl}
                />
                {schools.map((school, idx) =>
                    <CircleMarker
                        key={`school-${school.id}`}
                        color='black'
                        radius={10}
                        weight={2}
                        onClick={() => {
                            this.setState({activeSchool: school});
                        }}
                        center={school.coordinates}>
                    </CircleMarker>
                )}
                {this.state.activeSchool && <Popup
                    position={this.state.activeSchool.coordinates}
                    onClose={() => {
                        this.setState({activeSchool: null})
                    }}
                >
                    <MapTooltip school={this.state.activeSchool}/>
                </Popup>}
            </Map>
        ) : (
            "Data is loading..."
        );
    }
}
